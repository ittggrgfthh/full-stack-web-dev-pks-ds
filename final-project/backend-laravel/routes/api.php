<?php

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Auth\JWTController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Auth\RegeneratorOtpCodeController;
use App\Http\Controllers\CommentController;
use App\Models\Comment;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('profiles', ProfileController::class);
Route::apiResource('roles', RoleController::class);
Route::apiResource('posts', PostController::class);
Route::apiResource('comments', CommentController::class);

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
],function () {
    Route::post('register', RegisterController::class)->name('auth.register');
    Route::post('regenerate-otp-code', RegeneratorOtpCodeController::class)->name('auth.regenerate-otp-code');
    Route::post('verification', VerificationController::class)->name('auth.verification');
    Route::post('login', LoginController::class)->name('auth.login');
});



