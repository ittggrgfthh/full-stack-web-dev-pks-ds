<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserStoredEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $otp_code;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $otp_code)
    {
        $this->user = $user;
        $this->otp_code = $otp_code;
    }
}
