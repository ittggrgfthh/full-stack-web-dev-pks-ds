<?php

namespace App\Providers;

use App\Providers\UserStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendOtpToEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Providers\UserStoredEvent  $event
     * @return void
     */
    public function handle(UserStoredEvent $event)
    {
        //
    }
}
