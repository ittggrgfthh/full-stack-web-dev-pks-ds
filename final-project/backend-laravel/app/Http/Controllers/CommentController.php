<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    { 
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comment = Comment::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Comment Data Showed!',
            'data' => $comment
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();

        // $post = Post::create([
        //     'title' => $request->title,
        //     'description' => $request->description,
        // ]);

        $validator = Validator::make($requestAll, [
            'comment' => 'required',
            'post_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create($requestAll);

        if($comment){
            return response()->json([
                'success' => true,
                'message' => 'Comment Created!',
                'data' => $comment
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Create Comment Failed'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $comment 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $validator = Validator::make($request->all(),[
            'comment' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::findOrFail($comment->id);

        if($comment){

            $user = auth()->user();
            
            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'User salah'
                ], 403);
            }

            $comment->update([
                'comment' => $request->comment,
                'post_id' => $comment->post_id
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Comment Updated',
            'data' => $comment
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        if($comment){

            $user = auth()->user();
            
            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'User salah'
                ], 403);
            }
        
            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted'
            ], 200);
        }

        //if data not found
        return response()->json([
            'success' => false,
            'message' => 'Comment not Found'
        ], 404);
    }
}
