<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('log')->only('index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();
        // $requestAll = Profile::create([
        //     'name' => $request->name,
        //     'nim' => $request->nim,
        //     'address' => $request->address,
        //     'gender' => $request->gender,
        // ]);

        $validator = Validator::make($requestAll, [
            'name' => 'required',
            'nim' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $profile = Profile::create($requestAll);

        if($profile){
            return response()->json([
                'success' => true,
                'message' => 'Profile Created!',
                'data' => $profile
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Failed to create Profile!'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $profile 
        ], 200);
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        // dd($request);
        $requestAll = $request->all();

        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'nim' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $profile = Profile::findOrFail($profile->id);

        if($profile){

            $user = auth()->user();

            if($profile->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'User salah'
                ], 403);
            }

            $profile->update($requestAll);
        }

        return response()->json([
            'success' => true,
            'message' => 'Post Updated',
            'data' => $profile
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::findOrfail($id);

        if($profile){

            $user = auth()->user();
            
            if($profile->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'User salah'
                ], 403);
            }

            //delete post
            $profile->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted'
            ], 200);
        }

        //if data not found
        return response()->json([
            'success' => false,
            'message' => 'Post not Found'
        ], 404);
    }
}
