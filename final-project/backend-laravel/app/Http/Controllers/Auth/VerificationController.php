<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use App\Models\User;
use App\Models\OtpCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();

        $validator = Validator::make($requestAll, [
            'otp' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        if(!$otp_code){
            return response()->json([
                'success' => false,
                'message' => 'OTP code not found'
            ], 400);
        }

        $now = Carbon::now();

        if($now > $otp_code->valid_until){
            return response()->json([
                'success' => false,
                'message' => 'OTP code expired'
            ], 400);
        }

        $user = User::find($otp_code->user_id);

        $user->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();

        return response()->json([
            'success' => true,
            'message' => 'User Verified',
            'data' => $user
        ], 400);
    }
}
