<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // dd($request);
        $requestAll = $request->all();

        $validator = Validator::make($requestAll, [
            'email' => 'required',
            'password' => 'required',
        ]);
        // dd($validator->fails());
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $credentials = request(['email', 'password']);
        // dd(auth()->attempt($credentials));
        // dd($credentials);
        if (! $token = JWTAuth::attempt($credentials)) {
            return response()->json([
                'success' => false,
                'message' => 'Email atau Password anda salah'
            ], 401);
        }
        
        return response()->json([
            'success' => true,
            'message' => 'User Login Successfully',
            'data' => [
                'user' => auth()->user(),
                'token' => $token
            ]
        ], 401);
    }
}
