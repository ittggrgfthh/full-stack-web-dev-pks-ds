<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use App\Models\User;
use App\Models\OtpCode;
use Illuminate\Http\Request;
use App\Events\UserStoredEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $requestAll = $request->all();

        $validator = Validator::make($requestAll, [
            'username' => 'required|unique:users|regex:/^\S*$/u',
            'email' => 'required|unique:users,email|email',
            'password' => 'required',
            'no_wa' => 'required',
            'role_id' => 'required',
        ]);
        

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'no_wa' => $request->no_wa,
            'role_id' => $request->role_id,
        ]);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);

        //memanggil event UserStoredEvent
        UserStoredEvent::dispatch($user, $otp_code);

        return response()->json([
            'success' => true,
            'message' => 'User Created',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
