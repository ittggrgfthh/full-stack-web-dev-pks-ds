<?php

namespace App\Listeners;

use App\Models\User;
use App\Mail\SendOtpMail;
use App\Events\UserStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOtpToEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserStoredEvent  $event
     * @return void
     */
    public function handle(UserStoredEvent $event)
    {
        // dd($event);
        Mail::to($event->user->email)->send(new SendOtpMail($event));
    }
}
