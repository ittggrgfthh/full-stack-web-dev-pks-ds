<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    { 
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar Post berhasil ditampilkan',
            'data' => $posts 
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();
        // $post = Post::create([
        //     'title' => $request->title,
        //     'description' => $request->description,
        // ]);

        $validator = Validator::make($requestAll, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $post = Post::create($requestAll); 

        if($post){
            return response()->json([
                'success' => true,
                'message' => 'Post Data created',
                'data' => $post
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Create Post Data Failed'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data'    => $post 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $post = Post::findOrFail($post->id);

        if($post){

            $user = auth()->user();

            if($post->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'User salah'
                ], 403);
            }

            $post->update([
                'title' => $request->title,
                'description' => $request->description
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Post Updated',
            'data' => $post
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrfail($id);

        if($post){

            $user = auth()->user();
            
            if($post->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'User salah'
                ], 403);
            }

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted'
            ], 200);
        }

        //if data not found
        return response()->json([
            'success' => false,
            'message' => 'Post not Found'
        ], 404);
    }
}
