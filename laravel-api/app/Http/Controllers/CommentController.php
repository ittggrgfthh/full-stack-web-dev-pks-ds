<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Events\CommentStored;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    { 
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comment = Comment::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data' => $comment
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $requestAll = $request->all();

        // $post = Post::create([
        //     'title' => $request->title,
        //     'description' => $request->description,
        // ]);

        $validator = Validator::make($requestAll, [
            'content' => 'required',
            'post_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create($requestAll);
        
        // call event CommentStoredEvent
        event(new CommentStored($comment));

        if($comment){
            return response()->json([
                'success' => true,
                'message' => 'Comment Data created',
                'data' => $comment
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Create Post Data Failed'
        ], 409);
    }
        
    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        $comment = Comment::findOrfail($comment);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data'    => $comment 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $validator = Validator::make($request->all(),[
            'content' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::findOrFail($comment->id);

        if($comment){

            $user = auth()->user();
            
            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'User salah'
                ], 403);
            }

            $comment->update([
                'content' => $request->content,
                'post_id' => $comment->post_id
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Comment Updated',
            'data' => $comment
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);

        if($comment){

            $user = auth()->user();
            
            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'User salah'
                ], 403);
            }
        
            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted'
            ], 200);
        }

        //if data not found
        return response()->json([
            'success' => false,
            'message' => 'Comment not Found'
        ], 404);
    }
}
