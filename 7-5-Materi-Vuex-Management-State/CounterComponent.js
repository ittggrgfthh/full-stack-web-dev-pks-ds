const CounterComponent = {
  template: `
    <div>
        <p> State pada Component Counter {{ counter }} </p>
    </div>
`,
  computed: {
    counter() {
      //   console.log(this.$store);
      return this.$store.getters.counter;
    },
  },
};

export { CounterComponent };
