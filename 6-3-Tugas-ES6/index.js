// soal 1
const rectangular = (panjang, lebar) => ({
  panjang: panjang,
  lebar: lebar,
  luas: () => {
    console.log(`Luas Persegi Panjang : ${panjang * lebar}`);
  },
  keliling: () => {
    console.log(`Keliling Persegi Panjang : ${(panjang + lebar) * 2}`);
  },
});
rectangular(10, 16).luas();
rectangular(10, 16).keliling();

// soal 2
const newFunction = (firstName, lastName) => ({
  firstName,
  lastName,
  fullName() {
    console.log(`${firstName} ${lastName}`);
  },
});

//Driver Code
newFunction("William", "Imoh").fullName();

// soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

// Driver code
console.log(firstName, lastName, address, hobby);

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combinedEs6 = [...west, ...east];
//Driver Code
console.log(combinedEs6);

// soal 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`;
