<?php 

abstract class Hewan{
    public string $nama;
    public int $darah = 50;
    public int $jumlahKaki;
    public string $keahlian;

    public function atraksi($nama){
        echo "$nama sedang $this->keahlian" . PHP_EOL;
    }
}

abstract class Fight extends Hewan{
    public int $attackPower;
    public int $defencePower;

    public function serang($pelaku, $korban){
        echo "$pelaku sedang menyerang $korban" . PHP_EOL;
        $this->diserang($pelaku, $korban);
    }

    public function diserang($pelaku, $korban){
        echo "$korban sedang diserang $pelaku" . PHP_EOL;
    }
}

class Elang extends Fight{
    public string $jenisHewan = "Elang";
    public int $jumlahKaki = 2;
    public string $keahlian = "terbang tinggi";
    public int $attackPower = 10;
    public int $defencePower = 5;

    public function getInfoHewan($nama){
        echo "Nama Hewan = $nama" . PHP_EOL;
        echo "Jenis Hewan = $this->jenisHewan" . PHP_EOL;
        echo "Keahlian = $this->keahlian" . PHP_EOL;
        echo "Jumlah Kaki = $this->jumlahKaki" . PHP_EOL;
        echo "Darah = $this->darah" . PHP_EOL;
        echo "Attack Power = $this->attackPower" . PHP_EOL;
        echo "Defence Power = $this->defencePower" . PHP_EOL;
    }
}

class Harimau extends Fight{
    var $jenisHewan = "Harimau";
    public int $jumlahKaki = 4;
    public string $keahlian = "berlari cepat";
    public int $attackPower = 7;
    public int $defencePower = 8;

    public function getInfoHewan($nama){
        echo "Nama Hewan = $nama" . PHP_EOL;
        echo "Jenis Hewan = $this->jenisHewan" . PHP_EOL;
        echo "Keahlian = $this->keahlian" . PHP_EOL;
        echo "Jumlah Kaki = $this->jumlahKaki" . PHP_EOL;
        echo "Darah = $this->darah" . PHP_EOL;
        echo "Attack Power = $this->attackPower" . PHP_EOL;
        echo "Defence Power = $this->defencePower" . PHP_EOL;
    }
}